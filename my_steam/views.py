# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import Game
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import AddGameForm, UserRegistrationForm

class GameListView(LoginRequiredMixin,generic.ListView):
    model = Game
    context_object_name = 'game_list'

    def get_context_data(self, **kwargs):
        context = super(GameListView, self).get_context_data(**kwargs)
        context['extra_data'] = 'This data is just an example'
        return context

class UserGamesListView(LoginRequiredMixin, generic.ListView):
    model = Game
    context_object_name = 'game_list'

    def get_queryset(self):
        return Game.objects.filter(owner=self.request.user)

class GameDetailView(LoginRequiredMixin, generic.DetailView):
    model = Game
    paginate_by = 5

class GameCreate(LoginRequiredMixin, CreateView):
    model = Game
    form_class = AddGameForm

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(GameCreate, self).form_valid(form)

class GameUpdate(LoginRequiredMixin, UpdateView):
    model = Game
    fields = ['title','publisher','platform','cover_img','genre']

class GameDelete(LoginRequiredMixin, DeleteView):
    model = Game
    success_url = reverse_lazy('games')

@login_required
def index(request):
    num_games = Game.objects.all().count()
    return render(request,'index.html',context={'num_games':num_games})

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            email =  userObj['email']
            password =  userObj['password']
            if not (User.objects.filter(username=username).exists() or User.objects.filter(email=email).exists()):
                User.objects.create_user(username, email, password)
                user = authenticate(username = username, password = password)
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                raise forms.ValidationError('Looks like a username with that email or password already exists')
    else:
        form = UserRegistrationForm()
    return render(request, 'user_form.html', {'form' : form})
