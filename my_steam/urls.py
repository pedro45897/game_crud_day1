from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^signup/$', views.register, name='signup'),
    url(r'^games/$', views.GameListView.as_view(), name='games'),
    url(r'^my_games/$', views.UserGamesListView.as_view(), name='my_games'),
    url(r'^games/(?P<pk>\d+)$', views.GameDetailView.as_view(), name='game-detail'),
    url(r'^games/add/$', views.GameCreate.as_view(), name='add_game'),
    url(r'^games/(?P<pk>\d+)/update/$', views.GameUpdate.as_view(), name='update_game'),
    url(r'^games/(?P<pk>\d+)/delete/$', views.GameDelete.as_view(), name='delete_game'),
]
