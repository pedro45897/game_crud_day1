# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

class Game(models.Model):
    # Fields
    title = models.CharField(max_length=200)
    publisher = models.CharField(max_length=20, help_text='Max of 20 letters', null=True)
    platform = models.CharField(max_length=20, help_text='Max of 20 letters', null=True)
    cover_img = models.ImageField(upload_to='game_covers/', default=False)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    GENRES = (
        ('ACT','Action'),
        ('ADV','Adventure'),
        ('RPG','Role Play Game'),
        ('RCG','Racing'),
    )

    genre = models.CharField(max_length=20, help_text='Max of 20 letters', null=True,
        choices=GENRES)

    # Methods
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('game-detail', args=[str(self.id)])
